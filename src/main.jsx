import React from 'react'
import ReactDOM from 'react-dom'
import './styles/font-avo.css'
import './styles/font-gotham.css'
import './styles/index.css'
import App from './App'
import theme from './configs/theme'
import { ThemeProvider } from '@mui/material'


ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
