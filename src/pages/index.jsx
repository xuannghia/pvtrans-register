import React from "react";
import { Alert, AlertTitle, Box, Button, CardContent, CircularProgress, Container, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, Paper, TextField, Typography } from "@mui/material";
import BaseLayout from "../components/layouts/BaseLayout";
import axios from "axios";
import { API_ENDPOINT } from "../configs/constants";
import Heading from "../components/Heading";
import styled from "@emotion/styled";

const HomePage = () => {
  const [loading, setLoading] = React.useState(false);
  const [dialog, setDialog] = React.useState(false);
  const [error, setError] = React.useState(false)
  const [message, setMessage] = React.useState('')
  const [form, setForm] = React.useState({
    name: '',
    email: '',
    title: '',
    department: '',
    phone: ''
  })

  const handleSetForm = React.useCallback((e) => {
    setForm((state) => ({ ...state, [e.target.name]: e.target.value }))
  }, []);

  const handleSubmit = React.useCallback((e) => {
    setLoading(true)
    setMessage('')
    setError(false)
    e?.preventDefault();
    axios.post(API_ENDPOINT, form).then((res) => {
      setMessage('Đăng ký thành công!')
    }).catch((err) => {
      setMessage('Đăng ký không thành công!')
      setError(true)
    }).finally(() => {
      setDialog(true);
      setLoading(false)
    })
  }, [form])

  const handleCloseModal = () => {
    setDialog(false);
  }

  return (
    <BaseLayout>
      <Container>
        <Box mt={2}>
          <Heading>HỘI NGHỊ TỔNG KẾT</Heading>
          <Heading>HOẠT ĐỘNG SXKD NĂM 2021</Heading>
          <Heading>TRIỂN KHAI KẾ HOẠCH NĂM 2022</Heading>
          <Heading>VÀ HỘI NGHỊ NGƯỜI LAO ĐỘNG</Heading>
        </Box>
      </Container>
      <Container maxWidth="sm">
        <Box py={4}>
          <Paper variant="outlined">
            <CardContent>
              <form method="POST" onSubmit={handleSubmit}>
                <Box mb={2}>
                  <FormControl fullWidth >
                    <TextField
                      id="name"
                      name="name"
                      label="Họ tên"
                      variant="outlined"
                      required
                      value={form.name}
                      onChange={handleSetForm}
                    />
                  </FormControl>
                </Box>
                <Box mb={2}>
                  <FormControl fullWidth>
                    <TextField
                      id="title"
                      name="title"
                      label="Chức danh"
                      variant="outlined"
                      required
                      value={form.title}
                      onChange={handleSetForm}
                    />
                  </FormControl>
                </Box>
                <Box mb={2}>
                  <FormControl fullWidth>
                    <TextField
                      id="department"
                      name="department"
                      label="Phòng ban/ Dự án"
                      variant="outlined"
                      required
                      value={form.department}
                      onChange={handleSetForm}
                    />
                  </FormControl>
                </Box>
                <Box mb={2}>
                  <FormControl fullWidth>
                    <TextField
                      id="email"
                      name="email"
                      label="Email"
                      variant="outlined"
                      type="email"
                      required
                      value={form.email}
                      onChange={handleSetForm}
                    />
                  </FormControl>
                </Box>
                <Box mb={2}>
                  <FormControl fullWidth>
                    <TextField
                      id="phone"
                      name="phone"
                      label="Số điện thoại"
                      variant="outlined"
                      value={form.phone}
                      onChange={handleSetForm}
                    />
                  </FormControl>
                </Box>
                <Box>
                  <Button
                    color="secondary"
                    type="submit"
                    variant="contained"
                    fullWidth
                    disableElevation
                    size="large"
                    disabled={loading}
                  >
                    {loading ? 'Đang xử lý...' : 'Đăng ký'}
                  </Button>
                </Box>
              </form>
            </CardContent>
          </Paper>
        </Box>
        <Dialog
          open={dialog}
          onClose={handleCloseModal}
          fullWidth
          maxWidth="sm"
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle>Thông báo</DialogTitle>
          <DialogContent>
            <Alert severity={error ? "error" : "success"}>
              <AlertTitle>{message}</AlertTitle>
              {error ? 'Vui lòng kiểm tra lại thông tin.' : 'Vui lòng kiểm tra các hòm thư bao gồm thư rác (Junk mail) hoặc thư quảng cáo...'}
            </Alert>
          </DialogContent>
          <Typography textAlign="center" variant="body2">Hotline kỹ thuật: 0983532474 - Nghĩa</Typography>
          <DialogActions>
            <Button onClick={handleCloseModal} autoFocus>
              Đóng
            </Button>
          </DialogActions>
        </Dialog>
      </Container>
    </BaseLayout>
  )
}

export default HomePage;

const Br = styled.br`
  display: none;
  @media (max-width: 768px) {
    display: block;
  }
`
