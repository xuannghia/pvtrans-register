import styled from "@emotion/styled";
import { Box, Container } from "@mui/material";
import logoOfs from '../../assets/images/pv-ofs.svg'
import logoTld from '../../assets/images/tldld.svg'

const Header = () => {
  return (
    <HeaderWrapper>
      <Container>
        <LogoWrapperMobile>
          <HeaderLogoMobile><img src={logoOfs} alt="CÔNG TY DỊCH VỤ HÀNG HẢI DẦU KHÍ" /></HeaderLogoMobile>
          <HeaderLogoMobile><img src={logoTld} alt="TỔNG LĐ LAO ĐỘNG" /></HeaderLogoMobile>
        </LogoWrapperMobile>
        <Box display="flex" justifyContent="center" alignItems="center" pb={4} textAlign="center">
          <HeaderLogo><img src={logoOfs} alt="CÔNG TY DỊCH VỤ HÀNG HẢI DẦU KHÍ" /></HeaderLogo>
          <Box>
            <HeaderTitle>TỔNG CÔNG TY VẬN TẢI DẦU KHÍ</HeaderTitle>
            <HeaderName>CÔNG TY DỊCH VỤ HÀNG HẢI DẦU KHÍ</HeaderName>
          </Box>
          <HeaderLogo><img src={logoTld} alt="TỔNG LĐ LAO ĐỘNG" /></HeaderLogo>
        </Box>
      </Container>
    </HeaderWrapper>
  )
}

export default Header;

const HeaderTitle = styled.div`
  color: #1b1464;
  font-size: 22px;
  text-shadow: 0px 0px 3px #f6e5a3;
  @media (max-width: 768px) {
    font-size: 16px;
  }
`

const HeaderName = styled.div`
  color: #1b1464;
  font-size: 28px;
  font-weight: 700;
  text-shadow: 0px 0px 3px #f6e5a3;
  @media (max-width: 768px) {
    font-size: 22px;
  }
`

const HeaderWrapper = styled.div`
  position: relative;
  padding-left: 360px;
  @media (max-width: 768px) {
    padding-left: 0;
  }
`

const HeaderLogo = styled.div`
  height: 50px;
  width: 120px;
  margin-left: 12px;
  margin-right: 12px;
  @media (max-width: 768px) {
    display: none;
  }
`
const HeaderLogoMobile = styled.div`
  display: none;
  height: 50px;
  width: 120px;
  @media (max-width: 768px) {
    display: block;
  }
`

const LogoWrapperMobile = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 12px 0;
`
