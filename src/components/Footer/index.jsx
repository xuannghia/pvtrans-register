import styled from "@emotion/styled";
import { Box, Container } from "@mui/material";

const Footer = () => {
  return (
    <FooterWrapper>
      <Container>
        <Box py={4}>
          <div>TỔNG CÔNG TY CỔ PHẦN VẬN TẢI DẦU KHÍ</div>
          <div>CÔNG TY DỊCH VỤ HÀNG HẢI DẦU KHÍ</div>
        </Box>
      </Container>
    </FooterWrapper>
  )
}

export default Footer;

const FooterWrapper = styled.div`
  position: relative;
  margin-top: auto;
  background-color: #1b1464;
  color: #fff;
  text-align: center;
  padding-left: 360px;
  @media (max-width: 768px) {
    padding-left: 0;
  }
`
