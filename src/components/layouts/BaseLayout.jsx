import styled from "@emotion/styled";
import Footer from "../Footer";
import Header from "../Header";
import bgBadge from '../../assets/images/bg-badge.png';
import bgBadgeMobile from '../../assets/images/bg-badge-mobile-01.png';
import bg2 from '../../assets/images/bg.png';

const BaseLayout = ({ children }) => {
  return (
    <Layout>
      <Pattern>
        <img src={bgBadge} alt="pattern" />
      </Pattern>
      <Pattern isMobile>
        <img src={bgBadgeMobile} alt="pattern" />
      </Pattern>
      <Header />
      <main>{children}</main>
      <Footer />
    </Layout>
  )
}

export default BaseLayout;

const Layout = styled.div`
  position: relative;
  background-color: #f6e5a3;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  &:before {
    content: "";
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-image: url(${bg2});
    background-size: contain;
  }
`

const Pattern = styled.div`
  display: ${(props) => props.isMobile ? 'none' : 'block'};
  @media (max-width: 768px) {
    display: ${props => !props.isMobile ? 'none' : 'block'};
  }
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  img {
    object-fit: cover;
  }
`
