import styled from "@emotion/styled"

const Heading = styled.div`
  font-family: SVN-Gotham;
  text-align: center;
  font-size: 40px;
  line-height: 60px;
  font-weight: 700;
  color: orange;
  background-image: linear-gradient(to bottom, #b10609 0%, #d95a0e 100%);
  -webkit-background-clip: text;
  -webkit-text-stroke: 2px transparent;
  -webkit-text-stroke-width: 6px;
  text-shadow: 5px 2px 5px #580000;
  @media (max-width: 768px) {
    font-size: 20px;
    line-height: 32px;
    -webkit-text-stroke-width: 3px;
    margin: 0 -8px;
  }
`
export default Heading;
