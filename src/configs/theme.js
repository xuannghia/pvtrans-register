import { createTheme } from "@mui/material";

const theme = createTheme({
  palette: {
    primary: {
      main: '#cda300'
    },
    secondary: {
      main: '#f6e5a3'
    }
  }
});

export default theme;
