var S=Object.defineProperty,O=Object.defineProperties;var P=Object.getOwnPropertyDescriptors;var v=Object.getOwnPropertySymbols;var B=Object.prototype.hasOwnProperty,M=Object.prototype.propertyIsEnumerable;var A=(t,i,a)=>i in t?S(t,i,{enumerable:!0,configurable:!0,writable:!0,value:a}):t[i]=a,y=(t,i)=>{for(var a in i||(i={}))B.call(i,a)&&A(t,a,i[a]);if(v)for(var a of v(i))M.call(i,a)&&A(t,a,i[a]);return t},N=(t,i)=>O(t,P(i));import{n as l,j as e,C as f,a as o,B as d,R as s,b as W,P as K,c as V,F as x,T as g,d as H,D as z,e as j,f as q,A as U,g as Y,h as R,i as $,k as _,l as J,m as X}from"./vendor.e2f21df5.js";const Q=function(){const i=document.createElement("link").relList;if(i&&i.supports&&i.supports("modulepreload"))return;for(const n of document.querySelectorAll('link[rel="modulepreload"]'))h(n);new MutationObserver(n=>{for(const r of n)if(r.type==="childList")for(const p of r.addedNodes)p.tagName==="LINK"&&p.rel==="modulepreload"&&h(p)}).observe(document,{childList:!0,subtree:!0});function a(n){const r={};return n.integrity&&(r.integrity=n.integrity),n.referrerpolicy&&(r.referrerPolicy=n.referrerpolicy),n.crossorigin==="use-credentials"?r.credentials="include":n.crossorigin==="anonymous"?r.credentials="omit":r.credentials="same-origin",r}function h(n){if(n.ep)return;n.ep=!0;const r=a(n);fetch(n.href,r)}};Q();const Z=()=>e(ee,{children:e(f,{children:o(d,{py:4,children:[e("div",{children:"T\u1ED4NG C\xD4NG TY C\u1ED4 PH\u1EA6N V\u1EACN T\u1EA2I D\u1EA6U KH\xCD"}),e("div",{children:"C\xD4NG TY D\u1ECACH V\u1EE4 H\xC0NG H\u1EA2I D\u1EA6U KH\xCD"})]})})}),ee=l.div`
  position: relative;
  margin-top: auto;
  background-color: #1b1464;
  color: #fff;
  text-align: center;
  padding-left: 360px;
  @media (max-width: 768px) {
    padding-left: 0;
  }
`;var k="/assets/pv-ofs.9c297edf.svg",T="/assets/tldld.aa4fbe62.svg";const te=()=>e(ae,{children:o(f,{children:[o(re,{children:[e(F,{children:e("img",{src:k,alt:"C\xD4NG TY D\u1ECACH V\u1EE4 H\xC0NG H\u1EA2I D\u1EA6U KH\xCD"})}),e(F,{children:e("img",{src:T,alt:"T\u1ED4NG L\u0110 LAO \u0110\u1ED8NG"})})]}),o(d,{display:"flex",justifyContent:"center",alignItems:"center",pb:4,textAlign:"center",children:[e(w,{children:e("img",{src:k,alt:"C\xD4NG TY D\u1ECACH V\u1EE4 H\xC0NG H\u1EA2I D\u1EA6U KH\xCD"})}),o(d,{children:[e(ie,{children:"T\u1ED4NG C\xD4NG TY V\u1EACN T\u1EA2I D\u1EA6U KH\xCD"}),e(ne,{children:"C\xD4NG TY D\u1ECACH V\u1EE4 H\xC0NG H\u1EA2I D\u1EA6U KH\xCD"})]}),e(w,{children:e("img",{src:T,alt:"T\u1ED4NG L\u0110 LAO \u0110\u1ED8NG"})})]})]})}),ie=l.div`
  color: #1b1464;
  font-size: 22px;
  text-shadow: 0px 0px 3px #f6e5a3;
  @media (max-width: 768px) {
    font-size: 16px;
  }
`,ne=l.div`
  color: #1b1464;
  font-size: 28px;
  font-weight: 700;
  text-shadow: 0px 0px 3px #f6e5a3;
  @media (max-width: 768px) {
    font-size: 22px;
  }
`,ae=l.div`
  position: relative;
  padding-left: 360px;
  @media (max-width: 768px) {
    padding-left: 0;
  }
`,w=l.div`
  height: 50px;
  width: 120px;
  margin-left: 12px;
  margin-right: 12px;
  @media (max-width: 768px) {
    display: none;
  }
`,F=l.div`
  display: none;
  height: 50px;
  width: 120px;
  @media (max-width: 768px) {
    display: block;
  }
`,re=l.div`
  display: flex;
  justify-content: flex-end;
  padding: 12px 0;
`;var oe="/assets/bg-badge.b111d36c.png",le="/assets/bg-badge-mobile-01.4300a6f3.png",de="/assets/bg.d67c75d8.png";const se=({children:t})=>o(ue,{children:[e(G,{children:e("img",{src:oe,alt:"pattern"})}),e(G,{isMobile:!0,children:e("img",{src:le,alt:"pattern"})}),e(te,{}),e("main",{children:t}),e(Z,{})]}),ue=l.div`
  position: relative;
  background-color: #f6e5a3;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  &:before {
    content: "";
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-image: url(${de});
    background-size: contain;
  }
`,G=l.div`
  display: ${t=>t.isMobile?"none":"block"};
  @media (max-width: 768px) {
    display: ${t=>t.isMobile?"block":"none"};
  }
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  img {
    object-fit: cover;
  }
`,ce=(window==null?void 0:window.API_ENDPOINT)||"/",E=l.div`
  font-family: SVN-Gotham;
  text-align: center;
  font-size: 40px;
  line-height: 60px;
  font-weight: 700;
  color: orange;
  background-image: linear-gradient(to bottom, #b10609 0%, #d95a0e 100%);
  -webkit-background-clip: text;
  -webkit-text-stroke: 2px transparent;
  -webkit-text-stroke-width: 6px;
  text-shadow: 5px 2px 5px #580000;
  @media (max-width: 768px) {
    font-size: 20px;
    line-height: 32px;
    -webkit-text-stroke-width: 3px;
    margin: 0 -8px;
  }
`,he=()=>{const[t,i]=s.useState(!1),[a,h]=s.useState(!1),[n,r]=s.useState(!1),[p,b]=s.useState(""),[u,I]=s.useState({name:"",email:"",title:"",department:"",phone:""}),m=s.useCallback(c=>{I(D=>N(y({},D),{[c.target.name]:c.target.value}))},[]),L=s.useCallback(c=>{i(!0),b(""),r(!1),c==null||c.preventDefault(),W.post(ce,u).then(D=>{b("\u0110\u0103ng k\xFD th\xE0nh c\xF4ng!")}).catch(D=>{b("\u0110\u0103ng k\xFD kh\xF4ng th\xE0nh c\xF4ng!"),r(!0)}).finally(()=>{h(!0),i(!1)})},[u]),C=()=>{h(!1)};return o(se,{children:[e(f,{children:o(d,{mt:2,children:[e(E,{children:"H\u1ED8I NGH\u1ECA T\u1ED4NG K\u1EBET"}),e(E,{children:"HO\u1EA0T \u0110\u1ED8NG SXKD N\u0102M 2021"}),e(E,{children:"TRI\u1EC2N KHAI K\u1EBE HO\u1EA0CH N\u0102M 2022"}),e(E,{children:"V\xC0 H\u1ED8I NGH\u1ECA NG\u01AF\u1EDCI LAO \u0110\u1ED8NG"})]})}),o(f,{maxWidth:"sm",children:[e(d,{py:4,children:e(K,{variant:"outlined",children:e(V,{children:o("form",{method:"POST",onSubmit:L,children:[e(d,{mb:2,children:e(x,{fullWidth:!0,children:e(g,{id:"name",name:"name",label:"H\u1ECD t\xEAn",variant:"outlined",required:!0,value:u.name,onChange:m})})}),e(d,{mb:2,children:e(x,{fullWidth:!0,children:e(g,{id:"title",name:"title",label:"Ch\u1EE9c danh",variant:"outlined",required:!0,value:u.title,onChange:m})})}),e(d,{mb:2,children:e(x,{fullWidth:!0,children:e(g,{id:"department",name:"department",label:"Ph\xF2ng ban/ D\u1EF1 \xE1n",variant:"outlined",required:!0,value:u.department,onChange:m})})}),e(d,{mb:2,children:e(x,{fullWidth:!0,children:e(g,{id:"email",name:"email",label:"Email",variant:"outlined",type:"email",required:!0,value:u.email,onChange:m})})}),e(d,{mb:2,children:e(x,{fullWidth:!0,children:e(g,{id:"phone",name:"phone",label:"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i",variant:"outlined",value:u.phone,onChange:m})})}),e(d,{children:e(H,{color:"secondary",type:"submit",variant:"contained",fullWidth:!0,disableElevation:!0,size:"large",disabled:t,children:t?"\u0110ang x\u1EED l\xFD...":"\u0110\u0103ng k\xFD"})})]})})})}),o(z,{open:a,onClose:C,fullWidth:!0,maxWidth:"sm","aria-labelledby":"alert-dialog-title","aria-describedby":"alert-dialog-description",children:[e(j,{children:"Th\xF4ng b\xE1o"}),e(q,{children:o(U,{severity:n?"error":"success",children:[e(Y,{children:p}),n?"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin.":"Vui l\xF2ng ki\u1EC3m tra c\xE1c h\xF2m th\u01B0 bao g\u1ED3m th\u01B0 r\xE1c (Junk mail) ho\u1EB7c th\u01B0 qu\u1EA3ng c\xE1o..."]})}),e(R,{textAlign:"center",variant:"body2",children:"Hotline k\u1EF9 thu\u1EADt: 0983532474 - Ngh\u0129a"}),e($,{children:e(H,{onClick:C,autoFocus:!0,children:"\u0110\xF3ng"})})]})]})]})};l.br`
  display: none;
  @media (max-width: 768px) {
    display: block;
  }
`;function pe(){return e(he,{})}const me=_({palette:{primary:{main:"#cda300"},secondary:{main:"#f6e5a3"}}});J.render(e(s.StrictMode,{children:e(X,{theme:me,children:e(pe,{})})}),document.getElementById("root"));
